package edu.ntnu.idatt2001.wargames;

import edu.ntnu.idatt2001.wargames.enums.UnitType;
import edu.ntnu.idatt2001.wargames.exceptions.DuplicateUnitException;
import edu.ntnu.idatt2001.wargames.units.*;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Class ArmyTest for testing of class Army
 * @version 1.01 2022-03-01
 * @author Isak Jonsson
 */
class ArmyTest {
    Army testArmy = new Army("Test");
    Unit infantryTest = UnitFactory.makeUnit(UnitType.InfantryUnit,"InfantryTest",10);
    Unit infantryTest1 = new InfantryUnit("InfantryTest1", 10, 10, 0);
    Unit cavalryTest = UnitFactory.makeUnit(UnitType.CavalryUnit,"CavalryTest",10);
    Unit rangedTest = UnitFactory.makeUnit(UnitType.RangedUnit,"RangedTest",10);
    Unit commanderTest = UnitFactory.makeUnit(UnitType.CommanderUnit,"CommanderTest",10);
    List<Unit> testUnits = new ArrayList<>();
    Army testArmy1 = new Army("Test1", testUnits);

    /**
     * Test-method for method getName
     */
    @Test
    void testGetName() {
        assertEquals("Test",testArmy.getName());
    }

    /**
     * Test-method for method getAllUnits
     */
    @Test
    void testGetAllUnits() throws DuplicateUnitException{
        testUnits.add(infantryTest);
        testUnits.add(infantryTest1);
        testArmy.add(infantryTest);
        testArmy.add(infantryTest1);

        assertEquals(testUnits, testArmy.getAllUnits());
    }

    /**
     * Test-method for method add
     */
    @Test
    void testAdd() throws DuplicateUnitException{
        testArmy.add(infantryTest);
        assertEquals(infantryTest, testArmy.getAllUnits().get(0));
    }

    /**
     * Test that checks that DuplicateUnitException is thrown when adding an already existing unit with add method
     */
    @Test
    void testDuplicateUnitExceptionAdd() throws DuplicateUnitException {
        testArmy.add(infantryTest);
        assertThrows(DuplicateUnitException.class, () -> {testArmy.add(infantryTest);}, "DuplicateUnitException was expected");
    }

    /**
     * Test-method for method addAll
     */
    @Test
    void testAddAll() throws DuplicateUnitException{
        testUnits.add(infantryTest);
        testUnits.add(infantryTest1);
        testArmy.addAll(testUnits);
        assertEquals(testUnits, testArmy.getAllUnits());
    }

    /**
     * Test that checks that DuplicateUnitException is thrown when adding an already existing unit with add method
     */
    @Test
    void testDuplicateUnitExceptionAddAll() throws DuplicateUnitException{
        testUnits.add(infantryTest);
        testUnits.add(infantryTest1);
        testArmy.addAll(testUnits);

        assertThrows(DuplicateUnitException.class, () -> {testArmy.addAll(testUnits);}, "DuplicateUnitException was expected");
    }

    /**
     * Test-method for method remove
     */
    @Test
    void testRemove() throws DuplicateUnitException{
        testArmy.add(infantryTest);
        testArmy.add(infantryTest1);

        testArmy.remove(infantryTest);
        assertEquals(infantryTest1, testArmy.getAllUnits().get(0));
        //Checks that the program doesn't crash if you try to remove unit that isn't in the army
        testArmy.remove(commanderTest);
    }

    /**
     * Test-method for method hasUnits when true
     */
    @Test
    void testHasUnitsTrue() throws DuplicateUnitException{
        testArmy.add(infantryTest);
        assertTrue(testArmy.hasUnits());
    }

    /**
     * Test-method for method hasUnits when false
     */
    @Test
    void testHasUnitsFalse() {
        assertFalse(testArmy.hasUnits());
    }

    /**
     * Test-method for method getRandom
     * The method tests if getRandom returns at least one instance of each unit
     from 1000 tries from an army with one unit of each.
     */
    @Test
    void testGetRandom() throws DuplicateUnitException{
        testArmy.add(infantryTest);
        testArmy.add(infantryTest1);
        testArmy.add(rangedTest);
        testArmy.add(cavalryTest);
        testArmy.add(commanderTest);

        ArrayList<Unit> randoms = new ArrayList<>();
        boolean[] demands = {false, false, false, false, false};
        for (int i = 0; i < 1000; i++) {
            randoms.add(testArmy.getRandom());
        }

        demands[0] = randoms.stream().anyMatch(u -> u.equals(infantryTest));
        demands[1] = randoms.stream().anyMatch(u -> u.equals(infantryTest1));
        demands[2] = randoms.stream().anyMatch(u -> u.equals(rangedTest));
        demands[3] = randoms.stream().anyMatch(u -> u.equals(cavalryTest));
        demands[4] = randoms.stream().anyMatch(u -> u.equals(commanderTest));

        for (boolean b : demands) {
            assertTrue(b);
        }
    }

    /**
     * Test-method for method toString
     */
    @Test
    void testToString() throws DuplicateUnitException{
        testArmy.add(infantryTest);
        assertEquals("Army Test with 1 units.", testArmy.toString());
    }

    /**
     * Test-method for method equals
     */
    @Test
    void testEquals() {
        assertFalse(testArmy.equals(testArmy1));
    }

    /**
     * Test-method for method hashCode
     */
    @Test
    void testHashCode() {
        assertTrue(testArmy.hashCode() != testArmy1.hashCode());
    }

    /**
     * Test-method for method getInfantryUnits
     */
    @Test
    void testGetInfantryUnits() throws DuplicateUnitException{
        testUnits.add(infantryTest);
        testUnits.add(infantryTest1);
        testArmy.add(infantryTest);
        testArmy.add(infantryTest1);
        testArmy.add(cavalryTest);
        assertEquals(testUnits,testArmy.getInfantryUnits());
    }

    /**
     * Test-method for method getCavalryUnits
     */
    @Test
    void testGetCavalryUnits() throws DuplicateUnitException{
        testUnits.add(cavalryTest);
        testArmy.add(infantryTest);
        testArmy.add(infantryTest1);
        testArmy.add(cavalryTest);
        assertEquals(testUnits,testArmy.getCavalryUnits());
    }

    /**
     * Test-method for method getRangedUnits
     */
    @Test
    void testGetRangedUnits() throws DuplicateUnitException {
        testUnits.add(rangedTest);
        testArmy.add(infantryTest);
        testArmy.add(infantryTest1);
        testArmy.add(cavalryTest);
        testArmy.add(rangedTest);

        assertEquals(testUnits,testArmy.getRangedUnits());
    }

    /**
     * Test-method for method getCommanderUnits
     */
    @Test
    void testGetCommanderUnits() throws DuplicateUnitException {
        testUnits.add(commanderTest);
        testArmy.add(infantryTest);
        testArmy.add(infantryTest1);
        testArmy.add(cavalryTest);
        testArmy.add(commanderTest);

        assertEquals(testUnits,testArmy.getCommanderUnits());
    }

    /**
     * Test for filehandling that checks that all units in the army is as expected
     */
    @Test
    void testSaveAndReadFile() throws DuplicateUnitException{
        testUnits.addAll(UnitFactory.makeUnits(UnitType.InfantryUnit, "Footman", 100, 200));
        testUnits.addAll(UnitFactory.makeUnits(UnitType.CavalryUnit, "Knight", 100, 100));
        testUnits.addAll(UnitFactory.makeUnits(UnitType.RangedUnit, "Archer", 100, 200));
        testUnits.add(UnitFactory.makeUnit(UnitType.CommanderUnit, "Mountain King", 150));
        testArmy.addAll(testUnits);

        testArmy.saveToFile();
    
        File file = new File("src/main/resources/Test.csv");
        Army army = new Army(file);

        for (int i = 0; i < army.getAllUnits().size(); i++) {
            assertEquals(army.getAllUnits().get(i).toString(),testArmy.getAllUnits().get(i).toString());
        }
    }

    /**
     * Test for filehandling that checks that the army name is as expected
     */
    @Test
    void testSaveAndReadFileArmyName() throws DuplicateUnitException{
        testArmy.add(infantryTest);
        testArmy.add(infantryTest1);
        testArmy.add(cavalryTest);
        testArmy.add(commanderTest);

        testArmy.saveToFile();

        File file = new File("src/main/resources/Test.csv");
        Army army = new Army(file);

        assertEquals(army.getName(),testArmy.getName());
    }
}