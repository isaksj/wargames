package edu.ntnu.idatt2001.wargames;

import edu.ntnu.idatt2001.wargames.enums.Terrain;
import edu.ntnu.idatt2001.wargames.enums.UnitType;
import edu.ntnu.idatt2001.wargames.exceptions.DuplicateUnitException;
import edu.ntnu.idatt2001.wargames.units.*;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Class BattleTest for testing of class Battle
 * @version 1.01 2022-03-01
 * @author Isak Jonsson
 */
class BattleTest {
    Army testArmy = new Army("Human army1");
    Army testArmy1 = new Army("Human army2");
    Battle testBattle = new Battle(testArmy,testArmy1, Terrain.HILL);

    /**
     * Test-method for method simulate
     */
    @Test
    void simulate() throws DuplicateUnitException{
        ArrayList<Unit> testUnits = new ArrayList<>();
        testUnits.addAll(UnitFactory.makeUnits(UnitType.InfantryUnit,"Footman",100,500));
        testUnits.addAll(UnitFactory.makeUnits(UnitType.CavalryUnit,"Knight",100,100));
        testUnits.addAll(UnitFactory.makeUnits(UnitType.RangedUnit,"Archer",100,200));
        testUnits.add(UnitFactory.makeUnit(UnitType.CommanderUnit,"Mountain King", 100));

        ArrayList<Unit> testUnits1 = new ArrayList<>(UnitFactory.makeUnits(UnitType.CavalryUnit, "Knight", 100, 500));

        Army testArmy = new Army("Human army1",testUnits);
        Army testArmy1 = new Army("Human army2",testUnits1);
        Battle testBattle = new Battle(testArmy,testArmy1, Terrain.HILL);
        testBattle.simulate();

        Army testArmy2 = new Army("Test army1");
        Army testArmy3 = new Army("Test army2");
        testArmy2.add(new InfantryUnit("Footman", 100));

        Battle testBattle2 = new Battle(testArmy2,testArmy3, Terrain.HILL);
        assertEquals(testArmy2,testBattle2.simulate());
    }

    /**
     * Test-method for method toString
     */
    @Test
    void testToString() {
        assertEquals("Battle between Human army1 and Human army2.", testBattle.toString());
    }
}