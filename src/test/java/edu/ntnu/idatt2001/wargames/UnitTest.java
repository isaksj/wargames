package edu.ntnu.idatt2001.wargames;

import edu.ntnu.idatt2001.wargames.enums.UnitType;
import edu.ntnu.idatt2001.wargames.exceptions.InvalidHealthException;
import edu.ntnu.idatt2001.wargames.units.*;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 * Class UnitTests for testing of class edu.ntnu.idatt2001.wargames.Units.Unit and its subclasses
 * @version 1.01 2022-03-01
 * @author Isak Jonsson
 */
class UnitTest {
    Unit infantryTest = UnitFactory.makeUnit(UnitType.InfantryUnit,"InfantryTest",10);
    Unit infantryTest1 = new InfantryUnit("InfantryTest1", 10, 10, 0);
    Unit cavalryTest = UnitFactory.makeUnit(UnitType.CavalryUnit,"CavalryTest",10);
    Unit rangedTest = UnitFactory.makeUnit(UnitType.RangedUnit,"RangedTest",10);
    Unit commanderTest = UnitFactory.makeUnit(UnitType.CommanderUnit,"CommanderTest",10);

    /**
     * Test-method for method attack where the unit is expected to have zero health
     */
    @Test
    void testAttackZeroHealth() {
        infantryTest.attack(infantryTest1);
        assertEquals(0, infantryTest1.getHealth());
    }

    /**
     * Test-method for method attack where the unit is expected to have more health than zero
     */
    @Test
    void testAttackHealthNotZero() {
        infantryTest1.attack(infantryTest);
        // Expected result: 10 - (10 + 2) + (10 + 1) = 9;
        assertEquals(9, infantryTest.getHealth());
    }

    /**
     * Test-method for method getName
     */
    @Test
    void testGetName() {
        assertEquals("InfantryTest", infantryTest.getName());
    }

    /**
     * Test-method for method getHealth
     */
    @Test
    void testGetHealth() {
        assertEquals(10, infantryTest.getHealth());
    }

    /**
     * Test-method for method getAttack
     */
    @Test
    void testGetAttack() {
        assertEquals(15, infantryTest.getAttack());
    }

    /**
     * Test-method for method getArmor
     */
    @Test
    void testGetArmor() {
        assertEquals(10, infantryTest.getArmor());
    }

    /**
     * Test-method for method setHealth
     */
    @Test
    void testSetHealth() throws InvalidHealthException{
        infantryTest.setHealth(5);
        assertEquals(5, infantryTest.getHealth());
    }

    /**
     * Testing that a negative health is set to 0
     */
    @Test
    void testSetNegativeHealth() throws InvalidHealthException{
        infantryTest.setHealth(-1);
        assertEquals(0, infantryTest.getHealth());
    }

    /**
     * Test that checks that an InvalidHealthException is thrown when trying to increase a unit's health
     */
    @Test
    void testInvalidHealthException() {
        assertThrows(InvalidHealthException.class, () -> {infantryTest.setHealth(20);}, "InvalidHealthException was expected");
    }

    /**
     * Test-method for method toString
     */
    @Test
    void testToString() {
        assertEquals("InfantryTest with 10 health", infantryTest.toString());
    }

    /**
     * Test-method for method getAttackBonus when an infantry unit attacks
     */
    @Test
    void getAttackBonusInfantry() {
        assertEquals(2, infantryTest.getAttackBonus());
    }

    /**
     * Test-method for method getAttackBonus when a cavalry unit attacks the first time
     */
    @Test
    void getAttackBonusCavalryFirst() {
        assertEquals(6, cavalryTest.getAttackBonus());
    }

    /**
     * Test-method for method getAttackBonus when a cavalry unit attacks the second time
     */
    @Test
    void getAttackBonusCavalrySecond() {
        cavalryTest.getAttackBonus();
        assertEquals(2, cavalryTest.getAttackBonus());
    }

    /**
     * Test-method for method getAttackBonus when a ranged unit attacks
     */
    @Test
    void getAttackBonusRanged() {
        assertEquals(3, rangedTest.getAttackBonus());
    }

    /**
     * Test-method for method getAttackBonus when a commander unit attacks the first time
     */
    @Test
    void getAttackBonusCommanderFirst() {
        assertEquals(6, commanderTest.getAttackBonus());
    }

    /**
     * Test-method for method getAttackBonus when a commander unit attacks the second time
     */
    @Test
    void getAttackBonusCommanderSecond() {
        commanderTest.getAttackBonus();
        assertEquals(2, commanderTest.getAttackBonus());
    }

    /**
     * Test-method for method getResistBonus when an infantry unit is attacked
     */
    @Test
    void getResistBonusInfantry() {
        assertEquals(1, infantryTest.getResistBonus());
    }

    /**
     * Test-method for method getResistBonus when a cavalry unit is attacked
     */
    @Test
    void getResistBonusCavalry() {
        assertEquals(1, cavalryTest.getResistBonus());
    }

    /**
     * Test-method for method getResistBonus when a ranged unit is attacked the first time
     */
    @Test
    void getResistBonusRangedFirst() {
        assertEquals(6, rangedTest.getResistBonus());
    }

    /**
     * Test-method for method getResistBonus when a ranged unit is attacked the second time
     */
    @Test
    void getResistBonusRangedSecond() {
        rangedTest.getResistBonus();
        assertEquals(4, rangedTest.getResistBonus());
    }

    /**
     * Test-method for method getResistBonus when a ranged unit is attacked the third time
     */
    @Test
    void getResistBonusRangedThird() {
        rangedTest.getResistBonus();
        rangedTest.getResistBonus();
        assertEquals(2, rangedTest.getResistBonus());
    }

    /**
     * Test-method for method getResistBonus when a commander unit is attacked
     */
    @Test
    void getResistBonusCommander() {
        assertEquals(1, commanderTest.getResistBonus());
    }
}