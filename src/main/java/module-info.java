module Wargames {
    requires javafx.fxml;
    requires javafx.controls;
    exports edu.ntnu.idatt2001.wargames;
    exports edu.ntnu.idatt2001.wargames.gui;
    exports edu.ntnu.idatt2001.wargames.gui.controllers;
    opens edu.ntnu.idatt2001.wargames.gui.controllers;
    opens edu.ntnu.idatt2001.wargames;
    exports edu.ntnu.idatt2001.wargames.enums;
    opens edu.ntnu.idatt2001.wargames.enums;
}