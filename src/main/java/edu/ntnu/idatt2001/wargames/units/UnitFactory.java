package edu.ntnu.idatt2001.wargames.units;

import edu.ntnu.idatt2001.wargames.enums.UnitType;

import java.util.ArrayList;
import java.util.List;


/**
 * Factory class for units
 */
public abstract class UnitFactory {
    /**
     * Method that makes a new Unit
     * @param unitType The type of unit to be made as a String
     * @param name The name of unit to be made
     * @param health The health of unit to be made
     * @return The Unit that has been made
     */
    public static Unit makeUnit(UnitType unitType, String name, int health) {
        switch (unitType) {
            case RangedUnit -> {
                return new RangedUnit(name,health);
            }
            case CavalryUnit -> {
                return new CavalryUnit(name,health);
            }
            case InfantryUnit -> {
                return new InfantryUnit(name,health);
            }
            case CommanderUnit -> {
                return new CommanderUnit(name,health);
            }
        }

        return null;
    }

    /**
     * Method that makes a list of identical new Units
     * @param unitType The type of units to be made as a String
     * @param name The name of units to be made
     * @param health The health of units to be made
     * @return The list of units that has been made
     */
    public static List<Unit> makeUnits(UnitType unitType, String name, int health, int number) {
        List<Unit> unitList = new ArrayList<>();
        for (int i = 0; i < number; i++) {
            unitList.add(makeUnit(unitType,name,health));
        }
        return unitList;
    }
}
