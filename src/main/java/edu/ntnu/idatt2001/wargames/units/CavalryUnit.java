package edu.ntnu.idatt2001.wargames.units;

import edu.ntnu.idatt2001.wargames.enums.Terrain;

/**
 * Subclass CavalryUnit of Superclass Unit
 */

public class CavalryUnit extends Unit{
    private int attacks = 0;

    /**
     * Constructor that creates an instance of CavalryUnit
     * @param name Name of unit
     * @param health Health of unit
     * @param attack Value of the units attack
     * @param armor Value of the units armor
     */
    public CavalryUnit(String name, int health, int attack, int armor) {
        super(name, health, attack, armor);
    }

    public CavalryUnit(Unit that) {
        super(that);
    }

    /**
     * Constructor that creates an instance of CavalryUnit
     * Value of attack and armor is preset to respectively 20 and 12
     * @param name Name of unit
     * @param health Health of unit
     */
    public CavalryUnit(String name, int health) {
        super(name, health, 20, 12);
    }

    @Override
    public int getAttackBonus() {
        if (attacks == 0) {
            attacks++;
            return 6 + getTerrainBonusAttack();
        } else {
            return 2 + getTerrainBonusAttack();
        }
    }

    @Override
    public int getResistBonus() {
        return 1 + getTerrainBonusDefense();
    }

    @Override
    public void terrainBonus(Terrain terrain) {
        switch (terrain) {
            case PLAINS -> setTerrainBonusAttack(4);
            case FOREST -> setTerrainBonusDefense(-1);
        }
    }
}
