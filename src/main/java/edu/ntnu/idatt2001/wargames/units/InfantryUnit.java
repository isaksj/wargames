package edu.ntnu.idatt2001.wargames.units;

import edu.ntnu.idatt2001.wargames.enums.Terrain;

/**
 * Subclass InfantryUnit of Superclass Unit
 */

public class InfantryUnit extends Unit{

    /**
     * Constructor that creates an instance of InfantryUnit
     * @param name Name of unit
     * @param health Health of unit
     * @param attack Value of the units attack
     * @param armor Value of the units armor
     */
    public InfantryUnit(String name, int health, int attack, int armor) {
        super(name, health, attack, armor);
    }

    public InfantryUnit(Unit that) {
        super(that);
    }

    /**
     * Constructor that creates an instance of InfantryUnit
     * Value of attack and armor is preset to respectively 15 and 10
     * @param name Name of unit
     * @param health Health of unit
     */
    public InfantryUnit(String name, int health) {
        super(name, health, 15, 10);
    }

    @Override
    public int getAttackBonus() {
        return 2 + getTerrainBonusAttack();
    }

    @Override
    public int getResistBonus() {
        return 1 + getTerrainBonusDefense();
    }

    @Override
    public void terrainBonus(Terrain terrain) {
        switch (terrain) {
            case FOREST -> {
                setTerrainBonusAttack(3);
                setTerrainBonusDefense(3);
            }
        }
    }
}
