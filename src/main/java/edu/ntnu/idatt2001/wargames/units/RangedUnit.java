package edu.ntnu.idatt2001.wargames.units;

import edu.ntnu.idatt2001.wargames.enums.Terrain;

/**
 * Subclass RangedUnit of Superclass Unit
 */

public class RangedUnit extends Unit{
    private int defenses = 0;

    /**
     * Constructor that creates an instance of RangedUnit
     * @param name Name of unit
     * @param health Health of unit
     * @param attack Value of the units attack
     * @param armor Value of the units armor
     */
    public RangedUnit(String name, int health, int attack, int armor) {
        super(name, health, attack, armor);
    }

    public RangedUnit(Unit that) {
        super(that);
    }

    /**
     * Constructor that creates an instance of RangedUnit
     * Value of attack and armor is preset to respectively 15 and 8
     * @param name Name of unit
     * @param health Health of unit
     */
    public RangedUnit(String name, int health) {
        super(name, health, 15, 8);
    }

    @Override
    public int getAttackBonus() {
        return 3 + getTerrainBonusAttack();
    }

    @Override
    public int getResistBonus() {
        if (defenses == 0) {
            defenses += 1;
            return 6 + getTerrainBonusDefense();
        } else if (defenses == 1) {
            defenses += 1;
            return 4 + getTerrainBonusDefense();
        } else {
            return 2 + getTerrainBonusDefense();
        }
    }

    @Override
    public void terrainBonus(Terrain terrain) {
        switch (terrain) {
            case HILL -> setTerrainBonusAttack(4);
            case FOREST -> setTerrainBonusAttack(-1);
        }
    }
}
