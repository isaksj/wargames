package edu.ntnu.idatt2001.wargames.units;

import edu.ntnu.idatt2001.wargames.enums.Terrain;
import edu.ntnu.idatt2001.wargames.enums.UnitType;
import edu.ntnu.idatt2001.wargames.exceptions.InvalidHealthException;

/**
 * Superclass Unit
 */
public abstract class Unit {
    private final String name;
    private int health;
    private final int attack;
    private final int armor;
    private int terrainBonusAttack = 0;
    private int terrainBonusDefense = 0;

    /**
     * Constructor that creates an instance of Unit
     * @param name Name of unit
     * @param health Health of unit
     * @param attack Value of the units attack
     * @param armor Value of the units armor
     */
    public Unit(String name, int health, int attack, int armor) {
        this.name = name;
        this.health = health;
        this.attack = attack;
        this.armor = armor;
    }

    /**
     * Method that makes a deep copy of a unit
     * @param that The unit that is copied
     */
    public Unit(Unit that) {
        this(that.getName(),that.getHealth(),that.getAttack(),that.getArmor());
    }

    /**
     * Method that performs an attack from this to another unit
     * @param opponent The unit that is attacked
     */
    public void attack(Unit opponent) {
        int initialHealth = opponent.getHealth();
        int attack = getAttack() + getAttackBonus();
        int defense = opponent.getArmor() + opponent.getResistBonus();
        try {
            opponent.setHealth(initialHealth - attack + defense);
        } catch (InvalidHealthException e) {
            e.printStackTrace();
        }

    }


    /**
     * Getter for name attribute
     * @return Name of unit
     */
    public String getName() {
        return name;
    }


    /**
     * Getter for health attribute
     * @return Health of unit
     */
    public int getHealth() {
        return health;
    }

    /**
     * Getter for attack attribute
     * @return Value of the units attack
     */
    public int getAttack() {
        return attack;
    }

    /**
     * Getter for armor attribute
     * @return Value of the units armor
     */
    public int getArmor() {
        return armor;
    }


    /**
     * Setter for health attribute
     * @param health The value health attribute should be set as
     */
    public void setHealth(int health) throws InvalidHealthException {
        if (health < 0) {
            this.health = 0;
        } else if (this.health < health){
            throw new InvalidHealthException();
        } else{
            this.health = health;
        }
    }

    @Override
    public String toString() {
        return name + " with " + health + " health";
    }


    /**
     * Method that returns attack-bonus for a unit
     * @return Value of a specified units attack-bonus
     */
    public abstract int getAttackBonus();

    /**
     * Method that returns resist-bonus for a unit
     * @return Value of a specified units resist-bonus
     */
    public abstract int getResistBonus();

    /**
     * Method that sets the units terrain bonus
     * @param terrain The terrain for the battle
     */
    public abstract void terrainBonus(Terrain terrain);


    /**
     * Set method for the terrainBonusAttack variable
     * @param bonus The value that is set
     */
    protected void setTerrainBonusAttack(int bonus) {
        this.terrainBonusAttack = bonus;
    }

    /**
     * Set method for the terrainBonusDefense variable
     * @param bonus The value that is set
     */
    protected void setTerrainBonusDefense(int bonus) {
        this.terrainBonusDefense = bonus;
    }

    /**
     * Get method for the terrainBonusAttack variable
     * @return The value of terrainBonusAttack
     */
    protected int getTerrainBonusAttack() {
        return terrainBonusAttack;
    }

    /**
     * Get method for the terrainBonusDefense variable
     * @return The value of terrainBonusDefense
     */
    protected int getTerrainBonusDefense() {
        return terrainBonusDefense;
    }

    /**
     * Get method for the unitType
     * @return The unitType from the enum
     */
    public UnitType getType() {
        if (this instanceof InfantryUnit) {
            return UnitType.InfantryUnit;
        } else if (this instanceof CommanderUnit) {
            return UnitType.CommanderUnit;
        } else if (this instanceof RangedUnit) {
            return UnitType.RangedUnit;
        } else {
            return UnitType.CavalryUnit;
        }
    }
}
