package edu.ntnu.idatt2001.wargames.units;

/**
 * Subclass CommanderUnit of Superclass CavalryUnit
 */

public class CommanderUnit extends CavalryUnit {

    /**
     * Constructor that creates an instance of CommanderUnit
     * @param name Name of unit
     * @param health Health of unit
     * @param attack Value of the units attack
     * @param armor Value of the units armor
     */
    public CommanderUnit(String name, int health, int attack, int armor) {
        super(name, health, attack, armor);
    }

    /**
     * Constructor that creates an instance of CommanderUnit
     * Value of attack and armor is preset to respectively 25 and 15
     * @param name Name of unit
     * @param health Health of unit
     */
    public CommanderUnit(String name, int health) {
        super(name, health, 25, 15);
    }

    public CommanderUnit(Unit that) {
        super(that);
    }
}
