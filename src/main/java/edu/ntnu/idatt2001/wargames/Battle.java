package edu.ntnu.idatt2001.wargames;

import edu.ntnu.idatt2001.wargames.enums.Terrain;
import edu.ntnu.idatt2001.wargames.units.Unit;

/**
 * Class Battle
 */
public class Battle implements Runnable{
    private final Army armyOne;
    private final Army armyTwo;
    private Army winner;
    private boolean finishSim = false;

    /**
     * Constructor that creates an instance of Battle
     * @param armyOne One of the armies in the battle
     * @param armyTwo The other army in the battle
     * @param terrain Chosen terrain for the battle
     */
    public Battle(Army armyOne, Army armyTwo, Terrain terrain) {
        this.armyOne = armyOne;
        armyOne.setTerrain(terrain);
        this.armyTwo = armyTwo;
        armyTwo.setTerrain(terrain);
    }

    /**
     * Method that simulates the battle between the two armies set in the constructor
     * @return The army that wins the battle
     */
    public Army simulate() {
        while (armyOne.hasUnits() && armyTwo.hasUnits()) {
            Unit victim1 = armyTwo.getRandom();
            Unit victim2 = armyOne.getRandom();

            armyOne.getRandom().attack(victim1);
            armyTwo.getRandom().attack(victim2);

            if (victim1.getHealth() == 0) {
                armyTwo.remove(victim1);
            }
            if (victim2.getHealth() == 0) {
                armyOne.remove(victim2);
            }
        }

        if (armyOne.hasUnits()) {
            return armyOne;
        } else if (armyTwo.hasUnits()) {
            return armyTwo;
        } else {
            return null;
        }
    }

    @Override
    public String toString() {
        return "Battle between " + armyOne.getName() + " and " + armyTwo.getName() + ".";
    }

    /**
     * Thread method that is run when the thread is started.
     * It runs the simulation with some delays to be able to update the gui during the simulation of the battle.
     */
    @Override
    public void run() {
        while (armyOne.hasUnits() && armyTwo.hasUnits()) {
            Unit victim1 = armyTwo.getRandom();
            Unit victim2 = armyOne.getRandom();

            armyOne.getRandom().attack(victim1);
            armyTwo.getRandom().attack(victim2);

            if (victim1.getHealth() == 0) {
                armyTwo.remove(victim1);
            }
            if (victim2.getHealth() == 0) {
                armyOne.remove(victim2);
            }
            try {
                if (finishSim) {
                    //The simulation should not be delayed anymore so we do nothing...
                } else if (armyOne.getAllUnits().size() + armyTwo.getAllUnits().size() > 1000) {
                    Thread.sleep(3);
                } else if (armyOne.getAllUnits().size() + armyTwo.getAllUnits().size() > 500) {
                    Thread.sleep(5);
                } else if (armyOne.getAllUnits().size() + armyTwo.getAllUnits().size() > 250) {
                    Thread.sleep(10);
                } else if (armyOne.getAllUnits().size() + armyTwo.getAllUnits().size() > 100) {
                    Thread.sleep(15);
                } else if (armyOne.getAllUnits().size() + armyTwo.getAllUnits().size() > 50) {
                    Thread.sleep(25);
                } else {
                    Thread.sleep(40);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        if (armyOne.hasUnits()) {
            winner = armyOne;
        } else if (armyTwo.hasUnits()) {
            winner = armyTwo;
        } else {
            winner = null;
        }
    }

    /**
     * Set method for the instance variable finishSim.
     * If finishSim the delay in the simulation is stopped.
     * @param finishSim The value that is set.
     */
    public void setFinishSim(boolean finishSim) {
        this.finishSim = finishSim;
    }
}
