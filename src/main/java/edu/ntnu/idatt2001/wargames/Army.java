package edu.ntnu.idatt2001.wargames;

import edu.ntnu.idatt2001.wargames.enums.Terrain;
import edu.ntnu.idatt2001.wargames.enums.UnitType;
import edu.ntnu.idatt2001.wargames.exceptions.DuplicateUnitException;
import edu.ntnu.idatt2001.wargames.units.*;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Random;
import java.util.stream.Collectors;


/**
 * Class Army
 */
public class Army {
    private String name;
    private List<Unit> units;

    /**
     * Constructor that creates an instance of Army
     * Creates a new and empty list of units
     * @param name Name of army
     */
    public Army(String name) {
        this.name = name;
        units = new ArrayList<>();
    }

    /**
     * Constructor that creates an instance of Army
     * @param name Name of army
     * @param units List of units in the army
     */
    public Army(String name, List<Unit> units) {
        this.name = name;
        this.units = units;
    }

    /**
     * Constructor that creates a copy of Army
     * @param that
     */
    public Army(Army that) {
        this(that.getName(), that.copy());
    }

    /**
     * Constructor that creates an instance of Army from a file
     * @param file File that the army will be created from
     */
    public Army(File file) {
        try (FileReader fileReader = new FileReader(file)) {
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            String armyName = "";
            UnitType unitType;
            String unitName;
            String unitHealth;
            List<Unit> units = new ArrayList<>();
            boolean firstLine = true;
            String line;

            while ((line = bufferedReader.readLine()) != null) {
                if (!firstLine) {
                    String[] splitLine = line.split(",");
                    if (splitLine[0].equals("InfantryUnit")) {
                        unitType = UnitType.InfantryUnit;
                    } else if (splitLine[0].equals("CavalryUnit")) {
                        unitType = UnitType.InfantryUnit;
                    } else if (splitLine[0].equals("RangedUnit")) {
                        unitType = UnitType.RangedUnit;
                    } else {
                        unitType = UnitType.CommanderUnit;
                    }
                    unitName = splitLine[1];
                    unitHealth = splitLine[2];

                    units.add(UnitFactory.makeUnit(unitType,unitName,Integer.parseInt(unitHealth)));
                } else {
                    armyName = line;
                    firstLine = false;
                }
            }
            this.name = armyName;
            this.units = units;
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Getter for name attribute
     * @return Name of army
     */
    public String getName() {
        return name;
    }

    /**
     * Getter for units attribute
     * @return List of all units in the army
     */
    public List<Unit> getAllUnits() {
        return units;
    }

    /**
     * Method that adds a new unit to the army
     * @param unit Unit that is added to the army
     */
    public void add(Unit unit) throws DuplicateUnitException {
        if (this.units.contains(unit)) {
            throw new DuplicateUnitException(unit);
        }
        units.add(unit);
    }

    /**
     * Method that adds a list of new units to the army
     * @param units List of units that is added to the army
     */
    public void addAll(List<Unit> units) throws DuplicateUnitException{
        for (Unit u : units) {
            if (this.units.contains(u)) {
                throw new DuplicateUnitException(u);
            }
        }
        this.units.addAll(units);
    }

    /**
     * Method that removes a unit from the army
     * @param unit Unit that is removed from the army
     */
    public void remove(Unit unit) {
        units.remove(unit);
    }

    /**
     * Method checks if the army contains units
     * @return Boolean value of weather the army contains units or not
     */
    public boolean hasUnits() {
        return !units.isEmpty();
    }

    /**
     * Method that returns a random unit from the army
     * @return A random unit from the army
     */
    public Unit getRandom() {
        Random random = new Random();
        return units.get(random.nextInt(units.size()));
    }

    @Override
    public String toString() {
        return "Army " + name + " with " + units.size() + " units.";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Army army = (Army) o;
        return name.equals(army.name) && units.equals(army.units);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, units);
    }

    /**
     * Method that returns all InfantryUnits in the army
     * @return List of InfantryUnits in the army
     */
    public List<Unit> getInfantryUnits() {
        return units.stream().filter(unit -> unit instanceof InfantryUnit).collect(Collectors.toList());
    }

    /**
     * Method that returns all CavalryUnits in the army
     * @return List of CavalryUnits in the army
     */
    public List<Unit> getCavalryUnits() {
        return units.stream().filter(unit -> unit instanceof CavalryUnit && !(unit instanceof CommanderUnit)).collect(Collectors.toList());
    }

    /**
     * Method that returns all RangedUnits in the army
     * @return List of RangedUnits in the army
     */
    public List<Unit> getRangedUnits() {
        return units.stream().filter(unit -> unit instanceof RangedUnit).collect(Collectors.toList());
    }

    /**
     * Method that returns all CommanderUnits in the army
     * @return List of CommanderUnits in the army
     */
    public List<Unit> getCommanderUnits() {
        return units.stream().filter(unit -> unit instanceof CommanderUnit).collect(Collectors.toList());
    }

    /**
     * Method to save the army to a file
     */
    public void saveToFile() {
        FileWriter fileWriter = null;
        try {
            fileWriter = new FileWriter("src/main/resources/" + getName() + ".csv");

            fileWriter.write(getName());

            for (Unit u : units) {
                fileWriter.write("\n" + u.getClass().getName().substring(34) + ",");
                fileWriter.write(u.getName() + ",");
                fileWriter.write(String.valueOf(u.getHealth()));
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                fileWriter.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Method to save the army to a file with specified path for saving
     * @param path The path the file should be saved in
     */
    public void saveToFile(String path) {
        FileWriter fileWriter = null;
        try {
            fileWriter = new FileWriter(path + ".csv");

            fileWriter.write(getName());

            for (Unit u : units) {
                fileWriter.write("\n" + u.getClass().getName().substring(6) + ",");
                fileWriter.write(u.getName() + ",");
                fileWriter.write(String.valueOf(u.getHealth()));
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                fileWriter.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Method that sets the units terrainbonuses
     * @param terrain The terrain that has been set
     */
    public void setTerrain(Terrain terrain) {
        for (Unit u : units) {
            u.terrainBonus(terrain);
        }
    }

    /**
     * Method that calculates the total health of the entire army
     * @return The total health
     */
    public int totalHealth() {
        int sum = 0;
        for (Unit u : getAllUnits()) {
            sum += u.getHealth();
        }
        return sum;
    }

    /**
     * Method that calculates the total health of all cavalryUnits
     * @return The total health
     */
    public int totalCavalryHealth() {
        int sum = 0;
        for (Unit u : getCavalryUnits()) {
            sum += u.getHealth();
        }
        return sum;
    }

    /**
     * Method that calculates the total health of all infantryUnits
     * @return The total health
     */
    public int totalInfantryHealth() {
        int sum = 0;
        for (Unit u : getInfantryUnits()) {
            sum += u.getHealth();
        }
        return sum;
    }

    /**
     * Method that calculates the total health of all rangedUnits
     * @return The total health
     */
    public int totalRangedHealth() {
        int sum = 0;
        for (Unit u : getRangedUnits()) {
            sum += u.getHealth();
        }
        return sum;
    }

    /**
     * Method that calculates the total health of all commanderUnits
     * @return The total health
     */
    public int totalCommanderHealth() {
        int sum = 0;
        for (Unit u : getCommanderUnits()) {
            sum += u.getHealth();
        }
        return sum;
    }

    /**
     * Method that makes a deep copy of the list of units
     * @return Deep copy of the list of units
     */
    public List<Unit> copy() {
        ArrayList<Unit> copy = new ArrayList<>();
        for (Unit u : this.getAllUnits()) {
            if (u instanceof CavalryUnit && !(u instanceof CommanderUnit)) {
                copy.add(new CavalryUnit(u));
            } else if (u instanceof InfantryUnit) {
                copy.add(new InfantryUnit(u));
            } else if (u instanceof RangedUnit) {
                copy.add(new RangedUnit(u));
            } else if (u instanceof CommanderUnit) {
                copy.add(new CommanderUnit(u));
            }
        }
        return copy;
    }
}
