package edu.ntnu.idatt2001.wargames.exceptions;

/**
 * Custom exception that is thrown when someone tries to increase a units health
 */
public class InvalidHealthException extends Exception{
    /**
     * Constructor that passes a message
     */
    public InvalidHealthException() {
        super("The units health cant increase");
    }
}
