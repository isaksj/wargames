package edu.ntnu.idatt2001.wargames.exceptions;

import edu.ntnu.idatt2001.wargames.units.Unit;

/**
 * Custom exception that is thrown when someone tries to add a duplicate unit to an army
 */
public class DuplicateUnitException extends Exception{
    /**
     * Constructor that passes a message
     */
    public DuplicateUnitException(Unit u) {
        super("The unit " + u.toString() + " is already in the army");
    }
}
