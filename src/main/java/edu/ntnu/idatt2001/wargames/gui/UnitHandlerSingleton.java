package edu.ntnu.idatt2001.wargames.gui;

import edu.ntnu.idatt2001.wargames.units.Unit;

/**
 * Singleton for storing units. This Singleton can be used for gui applications
 * where moving units between scenes is necessary.
 */
public class UnitHandlerSingleton {
    private static Unit unit1;
    private static Unit unit2;
    private static boolean delete = false;

    /**
     * Get method for the delete-boolean
     * @return The value of delete
     */
    public static boolean isDelete() {
        return delete;
    }

    /**
     * Set method for the delete-boolean
     * @param delete The value that is set
     */
    public static void setDelete(boolean delete) {
        UnitHandlerSingleton.delete = delete;
    }

    /**
     * Get method for unit1
     * @return The unit in unit1
     */
    public static Unit getUnit1() {
        return unit1;
    }

    /**
     * Set method for unit1
     * @param u The unit that is set
     */
    public static void setUnit1(Unit u) {
        unit1 = u;
    }

    /**
     * Get method for unit2
     * @return The unit in unit2
     */
    public static Unit getUnit2() {
        return unit2;
    }

    /**
     * Set method for unit2
     * @param u The unit that is set
     */
    public static void setUnit2(Unit u) {
        unit2 = u;
    }
}
