package edu.ntnu.idatt2001.wargames.gui;

import edu.ntnu.idatt2001.wargames.Army;
import edu.ntnu.idatt2001.wargames.enums.UnitType;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

/**
 * Stores Army data per unit type in format suited for the army tables in the GUI
 */
public class ArmyTableData {
    private final SimpleStringProperty type;
    private SimpleIntegerProperty total;
    private SimpleIntegerProperty totalHealth;

    /**
     * Constructor added to create the model from the Army object
     * @param army Army that the model is created from
     * @param unitType The unit type the model is created from
     */
    public ArmyTableData(Army army, UnitType unitType) {
        this.type = new SimpleStringProperty(unitType + "s");

        switch (unitType) {
            case CavalryUnit -> {
                this.total = new SimpleIntegerProperty(army.getCavalryUnits().size());
                this.totalHealth = new SimpleIntegerProperty(army.totalCavalryHealth());
            }
            case RangedUnit -> {
                this.total = new SimpleIntegerProperty(army.getRangedUnits().size());
                this.totalHealth = new SimpleIntegerProperty(army.totalRangedHealth());
            }
            case InfantryUnit -> {
                this.total = new SimpleIntegerProperty(army.getInfantryUnits().size());
                this.totalHealth = new SimpleIntegerProperty(army.totalInfantryHealth());
            }
            case CommanderUnit -> {
                this.total = new SimpleIntegerProperty(army.getCommanderUnits().size());
                this.totalHealth = new SimpleIntegerProperty(army.totalCommanderHealth());
            }
        }
    }

    /**
     * Method that returns the unit type
     * @return unit type as a string
     */
    public String getType() {
        return type.get();
    }

    /**
     * Method that returns unit type
     * @return unit type as a SimpleStringProperty
     */
    public SimpleStringProperty typeProperty() {
        return type;
    }

    /**
     * Method that changes the unit type
     * @param type Unit type that is set
     */
    public void setType(String type) {
        this.type.set(type);
    }

    /**
     * Get method for the total units of the unit type
     * @return The total as an int
     */
    public int getTotal() {
        return total.get();
    }

    /**
     * Get method for the total units of the unit type
     * @return The total as a SimpleIntegerProperty
     */
    public SimpleIntegerProperty totalProperty() {
        return total;
    }

    /**
     * Get method for the total health of the units of the unit type
     * @return The total health as an int
     */
    public int getTotalHealth() {
        return totalHealth.get();
    }

    /**
     * Get method for the total health of the units of the unit type
     * @return The total health as a SimpleIntegerProperty
     */
    public SimpleIntegerProperty totalHealthProperty() {
        return totalHealth;
    }
}
