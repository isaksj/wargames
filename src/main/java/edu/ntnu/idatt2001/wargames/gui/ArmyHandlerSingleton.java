package edu.ntnu.idatt2001.wargames.gui;

import edu.ntnu.idatt2001.wargames.Army;

/**
 * Singleton for storing armies. This Singleton can be used for gui applications
 * where moving armies between scenes is necessary.
 */
public class ArmyHandlerSingleton {
    private static Army army1 = new Army("Empty army");
    private static Army army2 = new Army("Empty army");
    private static Army armyCopy1 = null;
    private static Army armyCopy2 = null;

    /**
     * Set method for the army1 variable
     * @param army The army that is set
     */
    public static void setArmy1(Army army) {
        army1 = army;
    }

    /**
     * Set method for the army2 variable
     * @param army The army that is set
     */
    public static void setArmy2(Army army) {
        army2 = army;
    }

    /**
     * Get method for the army1 variable
     * @return The army in army1
     */
    public static Army getArmy1() {
        return army1;
    }

    /**
     * Get method for the army2 variable
     * @return The army in army2
     */
    public static Army getArmy2() {
        return army2;
    }

    /**
     * Get method for the armyCopy1 variable
     * @return The army in armyCopy1
     */
    public static Army getArmyCopy1() {
        return armyCopy1;
    }

    /**
     * Set method for the armyCopy1 variable
     * @param army The army that is set
     */
    public static void setArmyCopy1(Army army) {
        ArmyHandlerSingleton.armyCopy1 = army;
    }

    /**
     * Get method for the armyCopy1 variable
     * @return The army in armyCopy1
     */
    public static Army getArmyCopy2() {
        return armyCopy2;
    }

    /**
     * Set method for the armyCopy2 variable
     * @param army The army that is set
     */
    public static void setArmyCopy2(Army army) {
        ArmyHandlerSingleton.armyCopy2 = army;
    }
}
