package edu.ntnu.idatt2001.wargames.gui;

import edu.ntnu.idatt2001.wargames.units.Unit;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

/**
 * Stores Unit data in format suited for the unit tables in the GUI
 */
public class UnitTableData {
    private final SimpleStringProperty type;
    private final SimpleStringProperty name;
    private final SimpleIntegerProperty health;

    /**
     * Constructor added to create the model from the Unit object
     * @param unit Unit that the model is created from
     */
    public UnitTableData(Unit unit) {
        this.type = new SimpleStringProperty(unit.getClass().getSimpleName());
        this.name = new SimpleStringProperty(unit.getName());
        this.health = new SimpleIntegerProperty(unit.getHealth());
    }

    /**
     * Method that returns the unit type
     * @return unit type as a string
     */
    public String getType() {
        return type.get();
    }


    /**
     * Method that returns unit type
     * @return unit type as a SimpleStringProperty
     */
    public SimpleStringProperty typeProperty() {
        return type;
    }


    /**
     * Method that changes the unit type
     * @param type Unit type that is set
     */
    public void setType(String type) {
        this.type.set(type);
    }

    /**
     * Get method for name
     * @return Name as a string
     */
    public String getName() {
        return name.get();
    }

    /**
     * Get method for name
     * @return Name as a SimpleStringProperty
     */
    public SimpleStringProperty nameProperty() {
        return name;
    }

    /**
     * Set method for name
     * @param name Name that is set
     */
    public void setName(String name) {
        this.name.set(name);
    }

    /**
     * Get method for health
     * @return Health as an int
     */
    public int getHealth() {
        return health.get();
    }

    /**
     * Get method for health
     * @return Health as a SimpleIntegerProperty
     */
    public SimpleIntegerProperty healthProperty() {
        return health;
    }

    /**
     * Set method for health
     * @param health Health that is set
     */
    public void setHealth(int health) {
        this.health.set(health);
    }
}
