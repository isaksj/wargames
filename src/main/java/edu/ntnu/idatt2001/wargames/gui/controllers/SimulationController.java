package edu.ntnu.idatt2001.wargames.gui.controllers;

import edu.ntnu.idatt2001.wargames.Army;
import edu.ntnu.idatt2001.wargames.Battle;
import edu.ntnu.idatt2001.wargames.enums.Terrain;
import edu.ntnu.idatt2001.wargames.enums.UnitType;
import edu.ntnu.idatt2001.wargames.gui.ArmyHandlerSingleton;
import edu.ntnu.idatt2001.wargames.gui.ArmyTableData;
import edu.ntnu.idatt2001.wargames.gui.GUI;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.util.Duration;

/**
 * Controller class for the FXML in the result page
 */
public class SimulationController {
    @FXML
    TableView<ArmyTableData> table1, table2;

    @FXML
    TableColumn<ArmyTableData, String> typeTable1, typeTable2;

    @FXML
    TableColumn<ArmyTableData, Integer> totalTable1, totalTable2, healthTable1, healthTable2;

    @FXML
    Label armyName1, armyName2, percentage1, percentage2, total1, totalHealth1, total2, totalHealth2;

    @FXML
    Button start;

    @FXML
    ProgressBar progress1, progress2;

    @FXML
    ChoiceBox choiceBox;

    private final ObservableList<ArmyTableData> data1 = FXCollections.observableArrayList();
    private final ObservableList<ArmyTableData> data2 = FXCollections.observableArrayList();

    /**
     * Initialize method that is run when the scene is set
     */
    @FXML
    private void initialize() {
        armyName1.setText("Army 1: " + ArmyHandlerSingleton.getArmy1().getName());
        armyName2.setText("Army 2: " + ArmyHandlerSingleton.getArmy2().getName());

        typeTable1.setCellValueFactory(
                new PropertyValueFactory<ArmyTableData, String>("type")
        );
        totalTable1.setCellValueFactory(
                new PropertyValueFactory<ArmyTableData, Integer>("total")
        );
        healthTable1.setCellValueFactory(
                new PropertyValueFactory<ArmyTableData, Integer>("totalHealth")
        );
        typeTable2.setCellValueFactory(
                new PropertyValueFactory<ArmyTableData, String>("type")
        );
        totalTable2.setCellValueFactory(
                new PropertyValueFactory<ArmyTableData, Integer>("total")
        );
        healthTable2.setCellValueFactory(
                new PropertyValueFactory<ArmyTableData, Integer>("totalHealth")
        );

        populate(ArmyHandlerSingleton.getArmy1(), data1);
        populate(ArmyHandlerSingleton.getArmy2(), data2);

        table1.setItems(data1);
        table2.setItems(data2);

        total1.setText(String.valueOf(ArmyHandlerSingleton.getArmy1().getAllUnits().size()));
        totalHealth1.setText(String.valueOf(ArmyHandlerSingleton.getArmy1().totalHealth()));
        total2.setText(String.valueOf(ArmyHandlerSingleton.getArmy2().getAllUnits().size()));
        totalHealth2.setText(String.valueOf(ArmyHandlerSingleton.getArmy2().totalHealth()));

        progress1.setProgress(1);
        progress2.setProgress(1);

        percentage1.setText(100 + "% left of the army");
        percentage2.setText(100 + "% left of the army");

        for (Terrain terrain : Terrain.values()) {
            choiceBox.getItems().add(terrain);
        }
        choiceBox.setValue("Choose terrain");
        choiceBox.setStyle("-fx-font-size: 16px");
    }

    /**
     * Method that handles events on the start simulation button
     * Sets the terrain, initializes the battle and simulates it
     */
    @FXML
    private void start() {
        if (choiceBox.getValue().equals("Choose terrain")) {
            Alert a = new Alert(Alert.AlertType.ERROR);
            a.setContentText("You have to choose a terrain before starting the simulation!");
            a.show();
            return;
        }
        Terrain terrain = (Terrain) choiceBox.getValue();

        ArmyHandlerSingleton.setArmyCopy1(new Army(ArmyHandlerSingleton.getArmy1()));
        ArmyHandlerSingleton.setArmyCopy2(new Army(ArmyHandlerSingleton.getArmy2()));

        Battle battle = new Battle(ArmyHandlerSingleton.getArmy1(),ArmyHandlerSingleton.getArmy2(), terrain);
        Thread thread = new Thread(battle);
        thread.start();
        Timeline timeline = new Timeline(
                new KeyFrame(Duration.millis(50), e -> {
                    update(battle);
                })
        );
        timeline.setCycleCount(Timeline.INDEFINITE);

        start.setText("Continue to results");
        start.setOnAction(actionEvent1 -> {
            battle.setFinishSim(true);
            timeline.stop();
            GUI.setSceneFromNode(start, "/gui/resultPage.fxml");
        });
        timeline.play();
    }

    /**
     * Method that populates the list that contains the ArmyTableData
     * @param army The Army that should be represented
     * @param data The list the unit models should be stored in
     */
    private void populate(final Army army, ObservableList<ArmyTableData> data) {
        data.clear();
        data.add(new ArmyTableData(army, UnitType.CavalryUnit));
        data.add(new ArmyTableData(army,UnitType.InfantryUnit));
        data.add(new ArmyTableData(army,UnitType.RangedUnit));
        data.add(new ArmyTableData(army,UnitType.CommanderUnit));
    }

    /**
     * Methods that updates the information about the armies during the simulation
     * @param battle The ongoing battle
     */
    private void update(Battle battle) {
        populate(ArmyHandlerSingleton.getArmy1(), data1);
        populate(ArmyHandlerSingleton.getArmy2(), data2);

        table1.setItems(data1);
        table2.setItems(data2);

        total1.setText(String.valueOf(ArmyHandlerSingleton.getArmy1().getAllUnits().size()));
        totalHealth1.setText(String.valueOf(ArmyHandlerSingleton.getArmy1().totalHealth()));
        total2.setText(String.valueOf(ArmyHandlerSingleton.getArmy2().getAllUnits().size()));
        totalHealth2.setText(String.valueOf(ArmyHandlerSingleton.getArmy2().totalHealth()));

        int percent1 = ArmyHandlerSingleton.getArmy1().totalHealth() * 100 / ArmyHandlerSingleton.getArmyCopy1().totalHealth();
        int percent2 = ArmyHandlerSingleton.getArmy2().totalHealth() * 100 / ArmyHandlerSingleton.getArmyCopy2().totalHealth();

        progress1.setProgress(percent1/100.0);
        progress2.setProgress(percent2/100.0);

        percentage1.setText(percent1 + "% left of the army");
        percentage2.setText(percent2 + "% left of the army");
    }
}
