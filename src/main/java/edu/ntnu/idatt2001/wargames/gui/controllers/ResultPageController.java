package edu.ntnu.idatt2001.wargames.gui.controllers;

import edu.ntnu.idatt2001.wargames.Army;
import edu.ntnu.idatt2001.wargames.gui.ArmyHandlerSingleton;
import edu.ntnu.idatt2001.wargames.gui.GUI;
import edu.ntnu.idatt2001.wargames.gui.UnitTableData;
import edu.ntnu.idatt2001.wargames.units.Unit;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.VBox;
import java.util.List;

/**
 * Controller class for the FXML in the result page
 */
public class ResultPageController {
    @FXML
    TableView<UnitTableData> table1, table2;

    @FXML
    TableColumn<UnitTableData, String> typeTable1, nameTable1, typeTable2, nameTable2;

    @FXML
    TableColumn<UnitTableData, Integer> healthTable1, healthTable2;

    @FXML
    Label armyName1, armyName2, winner1, winner2;

    @FXML
    TextArea armyDetails1, armyDetails2;

    @FXML
    Button saveToFile2, saveToFile1, loadFromFile1, loadFromFile2, simulation;

    @FXML
    VBox vBox1, vBox2;

    private final ObservableList<UnitTableData> data1 = FXCollections.observableArrayList();
    private final ObservableList<UnitTableData> data2 = FXCollections.observableArrayList();

    /**
     * Initialize method that is run when the scene is set
     */
    @FXML
    public void initialize() {
        typeTable1.setCellValueFactory(
                new PropertyValueFactory<UnitTableData, String>("type")
        );
        nameTable1.setCellValueFactory(
                new PropertyValueFactory<UnitTableData, String>("name")
        );
        healthTable1.setCellValueFactory(
                new PropertyValueFactory<UnitTableData, Integer>("health")
        );
        typeTable2.setCellValueFactory(
                new PropertyValueFactory<UnitTableData, String>("type")
        );
        nameTable2.setCellValueFactory(
                new PropertyValueFactory<UnitTableData, String>("name")
        );
        healthTable2.setCellValueFactory(
                new PropertyValueFactory<UnitTableData, Integer>("health")
        );

        update(ArmyHandlerSingleton.getArmy1(),ArmyHandlerSingleton.getArmy2());

        if (ArmyHandlerSingleton.getArmy1().getAllUnits().size() != 0) {
            winner1.setText("WINNER :)");
            winner2.setText("LOSER :(");
            vBox1.setStyle("-fx-background-color: #00ff00");
            vBox2.setStyle("-fx-background-color: #ff0000");
        } else if (ArmyHandlerSingleton.getArmy2().getAllUnits().size() != 0) {
            winner1.setText("LOSER :(");
            winner2.setText("WINNER :)");
            vBox1.setStyle("-fx-background-color: #ff0000");
            vBox2.setStyle("-fx-background-color: #00ff00");
        } else {
            winner1.setText("DEAD BUT NOT A LOSER");
            winner2.setText("DEAD BUT NOT A LOSER");
        }
    }

    /**
     * Methods that updates the information about the armies
     * @param army1 The first army
     * @param army2 The second army
     */
    private void update(Army army1, Army army2) {
        armyName1.setText("Army 1: " + army1.getName());
        armyName2.setText("Army 2: " + army2.getName());

        armyDetails1.setText(army1.getAllUnits().size() + " units total: " +
                army1.getInfantryUnits().size() + " Infantry units, " +
                army1.getRangedUnits().size() + " Ranged units,\n" +
                army1.getCavalryUnits().size() + " Cavalry units, " +
                army1.getCommanderUnits().size() + " Commander units");

        armyDetails2.setText(army2.getAllUnits().size() + " units total: " +
                army2.getInfantryUnits().size() + " Infantry units, " +
                army2.getRangedUnits().size() + " Ranged units,\n" +
                army2.getCavalryUnits().size() + " Cavalry units, " +
                army2.getCommanderUnits().size() + " Commander units");

        populate(army1.getAllUnits(), data1);
        populate(army2.getAllUnits(), data2);

        table1.setItems(data1);
        table2.setItems(data2);
    }

    /**
     * Method that populates the list that contains the UnitTableData
     * @param units The list of units that should be represented
     * @param data The list the unit models should be stored in
     */
    private void populate(final List<Unit> units, ObservableList<UnitTableData> data) {
        data.clear();
        units.forEach(u -> data.add(new UnitTableData(u)));
    }

    /**
     * Method that handles events on the reset simulation button
     * Resets the armies and sets the main page scene
     */
    @FXML
    private void resetSimulation() {
        ArmyHandlerSingleton.setArmy1(new Army(ArmyHandlerSingleton.getArmyCopy1()));
        ArmyHandlerSingleton.setArmy2(new Army(ArmyHandlerSingleton.getArmyCopy2()));
        ArmyHandlerSingleton.setArmyCopy1(null);
        ArmyHandlerSingleton.setArmyCopy1(null);
        GUI.setSceneFromNode(simulation, "/gui/mainPage.fxml");
    }
}
