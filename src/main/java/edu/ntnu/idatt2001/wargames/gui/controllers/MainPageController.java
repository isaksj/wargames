package edu.ntnu.idatt2001.wargames.gui.controllers;

import edu.ntnu.idatt2001.wargames.gui.ArmyHandlerSingleton;
import edu.ntnu.idatt2001.wargames.gui.GUI;
import edu.ntnu.idatt2001.wargames.gui.UnitHandlerSingleton;
import edu.ntnu.idatt2001.wargames.units.Unit;
import edu.ntnu.idatt2001.wargames.gui.UnitTableData;
import edu.ntnu.idatt2001.wargames.Army;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.util.Callback;

import java.io.File;
import java.util.List;

/**
 * Controller class for the FXML in the main page
 */
public class MainPageController {
    @FXML
    TableView<UnitTableData> table1, table2;

    @FXML
    TableColumn<UnitTableData, String> typeTable1, nameTable1, typeTable2, nameTable2;

    @FXML
    TableColumn<UnitTableData, Integer> healthTable1, healthTable2;

    @FXML
    Label armyName1, armyName2;

    @FXML
    TextArea armyDetails1, armyDetails2;

    @FXML
    Button saveToFile2, saveToFile1, loadFromFile1, loadFromFile2, addUnit1, addUnit2, simulation;

    private final ObservableList<UnitTableData> data1 = FXCollections.observableArrayList();
    private final ObservableList<UnitTableData> data2 = FXCollections.observableArrayList();

    /**
     * Initialize method that is run when the scene is set.
     * Initializes the tables and the buttons in them.
     */
    @FXML
    public void initialize() {
        typeTable1.setCellValueFactory(
                new PropertyValueFactory<UnitTableData, String>("type")
        );
        nameTable1.setCellValueFactory(
                new PropertyValueFactory<UnitTableData, String>("name")
        );
        healthTable1.setCellValueFactory(
                new PropertyValueFactory<UnitTableData, Integer>("health")
        );
        typeTable2.setCellValueFactory(
                new PropertyValueFactory<UnitTableData, String>("type")
        );
        nameTable2.setCellValueFactory(
                new PropertyValueFactory<UnitTableData, String>("name")
        );
        healthTable2.setCellValueFactory(
                new PropertyValueFactory<UnitTableData, Integer>("health")
        );

        TableColumn<UnitTableData, Void> colBtn1 = new TableColumn("");
        Callback<TableColumn<UnitTableData, Void>, TableCell<UnitTableData, Void>> cellFactory1 = new Callback<TableColumn<UnitTableData, Void>, TableCell<UnitTableData, Void>>() {
            @Override
            public TableCell<UnitTableData, Void> call(final TableColumn<UnitTableData, Void> param) {
                final TableCell<UnitTableData, Void> cell = new TableCell<UnitTableData, Void>() {

                    private final Button btn = new Button("Edit"); {
                        btn.setOnAction((ActionEvent event) -> {
                            UnitHandlerSingleton.setUnit2(ArmyHandlerSingleton.getArmy1().getAllUnits().get(getIndex()));
                            editUnit();
                            if (UnitHandlerSingleton.isDelete()) {
                                ArmyHandlerSingleton.getArmy1().getAllUnits().remove(getIndex());
                                UnitHandlerSingleton.setDelete(false);
                                update(ArmyHandlerSingleton.getArmy1(),ArmyHandlerSingleton.getArmy2());
                                return;
                            } else if (UnitHandlerSingleton.getUnit1() == null){
                                return;
                            }
                            ArmyHandlerSingleton.getArmy1().getAllUnits().set(getIndex(),UnitHandlerSingleton.getUnit1());
                            UnitHandlerSingleton.setUnit1(null);
                            update(ArmyHandlerSingleton.getArmy1(),ArmyHandlerSingleton.getArmy2());
                        });
                    }

                    @Override
                    public void updateItem(Void item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty) {
                            setGraphic(null);
                        } else {
                            setGraphic(btn);
                        }
                    }
                };
                return cell;
            }
        };
        colBtn1.setCellFactory(cellFactory1);
        table1.getColumns().add(colBtn1);

        TableColumn<UnitTableData, Void> colBtn2 = new TableColumn("");
        Callback<TableColumn<UnitTableData, Void>, TableCell<UnitTableData, Void>> cellFactory2 = new Callback<TableColumn<UnitTableData, Void>, TableCell<UnitTableData, Void>>() {
            @Override
            public TableCell<UnitTableData, Void> call(final TableColumn<UnitTableData, Void> param) {
                final TableCell<UnitTableData, Void> cell = new TableCell<UnitTableData, Void>() {

                    private final Button btn = new Button("Edit"); {
                        btn.setOnAction((ActionEvent event) -> {
                            UnitHandlerSingleton.setUnit2(ArmyHandlerSingleton.getArmy2().getAllUnits().get(getIndex()));
                            editUnit();
                            if (UnitHandlerSingleton.isDelete()) {
                                ArmyHandlerSingleton.getArmy2().getAllUnits().remove(getIndex());
                                UnitHandlerSingleton.setDelete(false);
                                update(ArmyHandlerSingleton.getArmy1(),ArmyHandlerSingleton.getArmy2());
                                return;
                            } else if (UnitHandlerSingleton.getUnit1() == null){
                                return;
                            }
                            ArmyHandlerSingleton.getArmy2().getAllUnits().set(getIndex(),UnitHandlerSingleton.getUnit1());
                            UnitHandlerSingleton.setUnit1(null);
                            update(ArmyHandlerSingleton.getArmy1(),ArmyHandlerSingleton.getArmy2());
                        });
                    }

                    @Override
                    public void updateItem(Void item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty) {
                            setGraphic(null);
                        } else {
                            setGraphic(btn);
                        }
                    }
                };
                return cell;
            }
        };
        colBtn2.setCellFactory(cellFactory2);
        table2.getColumns().add(colBtn2);

        typeTable1.setSortable(false);
        typeTable2.setSortable(false);
        nameTable1.setSortable(false);
        nameTable2.setSortable(false);
        healthTable1.setSortable(false);
        healthTable2.setSortable(false);
        colBtn1.setSortable(false);
        colBtn2.setSortable(false);
        table1.setSelectionModel(null);
        table2.setSelectionModel(null);

        update(ArmyHandlerSingleton.getArmy1(),ArmyHandlerSingleton.getArmy2());
    }

    /**
     * Methods that updates the information about the armies
     * @param army1 The first army
     * @param army2 The second army
     */
    private void update(Army army1, Army army2) {
        armyName1.setText("Army 1: " + army1.getName());
        armyName2.setText("Army 2: " + army2.getName());

        armyDetails1.setText(army1.getAllUnits().size() + " units total: " +
                army1.getInfantryUnits().size() + " Infantry units, " +
                army1.getRangedUnits().size() + " Ranged units,\n" +
                army1.getCavalryUnits().size() + " Cavalry units, " +
                army1.getCommanderUnits().size() + " Commander units");

        armyDetails2.setText(army2.getAllUnits().size() + " units total: " +
                army2.getInfantryUnits().size() + " Infantry units, " +
                army2.getRangedUnits().size() + " Ranged units,\n" +
                army2.getCavalryUnits().size() + " Cavalry units, " +
                army2.getCommanderUnits().size() + " Commander units");

        populate(army1.getAllUnits(), data1);
        populate(army2.getAllUnits(), data2);

        table1.setItems(data1);
        table2.setItems(data2);
    }

    /**
     * Method that populates the list that contains the UnitTableData
     * @param units The list of units that should be represented
     * @param data The list the unit models should be stored in
     */
    private void populate(final List<Unit> units, ObservableList<UnitTableData> data) {
        data.clear();
        units.forEach(u -> data.add(new UnitTableData(u)));
    }

    /**
     * Method that handles events on the save to file button in army 1.
     * Saves the current army to a file.
     */
    @FXML
    private void saveToFile1() {
        Alert a = new Alert(Alert.AlertType.NONE);
        try {
            FileChooser fileChooser = new FileChooser();
            Stage stage = (Stage) saveToFile1.getScene().getWindow();
            String path = fileChooser.showSaveDialog(stage).getPath();
            ArmyHandlerSingleton.getArmy1().saveToFile(path);
            a.setAlertType(Alert.AlertType.INFORMATION);
            a.setContentText("File saved successfully");
            a.show();
        } catch (Exception e) {
            a.setAlertType(Alert.AlertType.ERROR);
            a.setContentText("Nothing was saved");
            a.show();
        }
    }

    /**
     * Method that handles events on the save to file button in army 2.
     * Saves the current army to a file.
     */
    @FXML
    private void saveToFile2() {
        Alert a = new Alert(Alert.AlertType.NONE);
        try {
            FileChooser fileChooser = new FileChooser();
            Stage stage = (Stage) saveToFile2.getScene().getWindow();
            String path = fileChooser.showSaveDialog(stage).getPath();
            ArmyHandlerSingleton.getArmy2().saveToFile(path);
            a.setAlertType(Alert.AlertType.INFORMATION);
            a.setContentText("File saved successfully");
            a.show();
        } catch (Exception e) {
            a.setAlertType(Alert.AlertType.ERROR);
            a.setContentText("Nothing was saved");
            a.show();
        }
    }

    /**
     * Method that handles events on the load from file button i army 1.
     * Loads the army from a chosen file.
     */
    @FXML
    private void loadFromFile1() {
        Alert a = new Alert(Alert.AlertType.NONE);
        try {
            FileChooser fileChooser = new FileChooser();
            Stage stage = (Stage) loadFromFile1.getScene().getWindow();
            File file = fileChooser.showOpenDialog(stage);
            ArmyHandlerSingleton.setArmy1(new Army(file));
            update(ArmyHandlerSingleton.getArmy1(),ArmyHandlerSingleton.getArmy2());
            a.setAlertType(Alert.AlertType.INFORMATION);
            a.setContentText("File loaded successfully");
            a.show();
        } catch (Exception e) {
            a.setAlertType(Alert.AlertType.ERROR);
            a.setContentText("Loading failed");
            a.show();
        }
    }

    /**
     * Method that handles events on the load from file button i army 2.
     * Loads the army from a chosen file.
     */
    @FXML
    private void loadFromFile2() {
        Alert a = new Alert(Alert.AlertType.NONE);
        try {
            FileChooser fileChooser = new FileChooser();
            Stage stage = (Stage) loadFromFile2.getScene().getWindow();
            File file = fileChooser.showOpenDialog(stage);
            ArmyHandlerSingleton.setArmy2(new Army(file));
            update(ArmyHandlerSingleton.getArmy1(),ArmyHandlerSingleton.getArmy2());
            a.setAlertType(Alert.AlertType.INFORMATION);
            a.setContentText("File loaded successfully");
            a.show();
        } catch (Exception e) {
            a.setAlertType(Alert.AlertType.ERROR);
            a.setContentText("Loading failed");
            a.show();
        }
    }

    /**
     * Method that handles events on the add unit button i army 1.
     * Sets the dialog and handles errors.
     */
    @FXML
    private void addUnit1() {
        GUI.setDialogFromScene("/gui/addUnitDialog.fxml");
        Alert a = new Alert(Alert.AlertType.NONE);
        if (UnitHandlerSingleton.getUnit1() == null) {
            a.setAlertType(Alert.AlertType.ERROR);
            a.setContentText("The unit could not be added");
            a.show();
            return;
        }
        try {
            ArmyHandlerSingleton.getArmy1().add(UnitHandlerSingleton.getUnit1());
            update(ArmyHandlerSingleton.getArmy1(),ArmyHandlerSingleton.getArmy2());
            UnitHandlerSingleton.setUnit1(null);
            a.setAlertType(Alert.AlertType.INFORMATION);
            a.setContentText("Unit added successfully");
            a.show();
        } catch (Exception e) {
            UnitHandlerSingleton.setUnit1(null);
            a.setAlertType(Alert.AlertType.ERROR);
            a.setContentText("The unit could not be added");
            a.show();
        }
    }

    /**
     * Method that handles events on the add unit button i army 2.
     * Sets the dialog and handles errors.
     */
    @FXML
    private void addUnit2() {
        GUI.setDialogFromScene("/gui/addUnitDialog.fxml");
        Alert a = new Alert(Alert.AlertType.NONE);
        if (UnitHandlerSingleton.getUnit1() == null) {
            a.setAlertType(Alert.AlertType.ERROR);
            a.setContentText("No unit was added");
            a.show();
            return;
        }
        try {
            ArmyHandlerSingleton.getArmy2().add(UnitHandlerSingleton.getUnit1());
            update(ArmyHandlerSingleton.getArmy1(),ArmyHandlerSingleton.getArmy2());
            UnitHandlerSingleton.setUnit1(null);
            a.setAlertType(Alert.AlertType.INFORMATION);
            a.setContentText("Unit added successfully");
            a.show();
        } catch (Exception e) {
            UnitHandlerSingleton.setUnit1(null);
            a.setAlertType(Alert.AlertType.ERROR);
            a.setContentText("The unit could not be added");
            a.show();
        }
    }

    /**
     * Method that handles events on the edit buttons.
     * Sets the dialog and handles errors.
     */
    private void editUnit() {
        GUI.setDialogFromScene("/gui/editUnitDialog.fxml");
        Alert a = new Alert(Alert.AlertType.NONE);
        if (UnitHandlerSingleton.isDelete()) {
            a.setAlertType(Alert.AlertType.INFORMATION);
            a.setContentText("Unit deleted successfully");
            a.show();
            return;
        } else if (UnitHandlerSingleton.getUnit1() == null) {
            a.setAlertType(Alert.AlertType.ERROR);
            a.setContentText("The unit was not edited");
            a.show();
            return;
        }
        try {
            a.setAlertType(Alert.AlertType.INFORMATION);
            a.setContentText("Unit edited successfully");
            a.show();
        } catch (Exception e) {
            a.setAlertType(Alert.AlertType.ERROR);
            a.setContentText("The unit could not be edited");
            a.show();
        }
    }

    /**
     * Method that handles events on the start simulation button.
     * Sets the simulation scene.
     */
    @FXML
    private void simulate() {
        if (!ArmyHandlerSingleton.getArmy1().hasUnits() || !ArmyHandlerSingleton.getArmy2().hasUnits()) {
            Alert a = new Alert(Alert.AlertType.ERROR);
            a.setContentText("You can't start the simulation with an empty army!");
            a.show();
            return;
        }
        GUI.setSceneFromNode(simulation, "/gui/simulation.fxml");
    }
}
