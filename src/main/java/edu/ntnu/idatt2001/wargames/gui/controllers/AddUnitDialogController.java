package edu.ntnu.idatt2001.wargames.gui.controllers;

import edu.ntnu.idatt2001.wargames.enums.UnitType;
import edu.ntnu.idatt2001.wargames.gui.UnitHandlerSingleton;
import edu.ntnu.idatt2001.wargames.units.UnitFactory;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * Controller class for the FXML in the add unit dialog
 */
public class AddUnitDialogController {
    @FXML
    ChoiceBox unitType;

    @FXML
    TextField unitName, unitHealth;

    @FXML
    Button save;

    /**
     * Initialize method that is run when the scene is set.
     */
    @FXML
    private void initialize() {
        for (UnitType u : UnitType.values()) {
            unitType.getItems().add(u);
        }
        unitType.setValue("UnitType");
        unitType.setStyle("-fx-font-size: 16px");
        unitName.setText("Unit name");
        unitHealth.setText("10");
    }

    /**
     * Method that handles events on the save button.
     * Saves the unit and closes the dialog.
     */
    @FXML
    private void save() {
        if (unitType.getValue().equals("UnitType")) {
            Alert a = new Alert(Alert.AlertType.ERROR);
            a.setContentText("You must select a unit type!");
            a.show();
            return;
        }
        UnitType type = (UnitType) unitType.getValue();
        String name = unitName.getText();
        Integer health;
        try {
            health = Integer.parseInt(unitHealth.getText());
        } catch (NumberFormatException e) {
            Alert a = new Alert(Alert.AlertType.ERROR);
            a.setContentText("The unit health must be a number");
            a.show();
            return;
        }
        if (health <= 0) {
            Alert a = new Alert(Alert.AlertType.ERROR);
            a.setContentText("The unit health must be greater than 0");
            a.show();
            return;
        }
        UnitHandlerSingleton.setUnit1(UnitFactory.makeUnit(type,name,health));
        unitType.setValue("UnitType");
        Stage stage  = (Stage) save.getScene().getWindow();
        stage.close();
    }
}
