package edu.ntnu.idatt2001.wargames.gui.controllers;

import edu.ntnu.idatt2001.wargames.enums.UnitType;
import edu.ntnu.idatt2001.wargames.gui.UnitHandlerSingleton;
import edu.ntnu.idatt2001.wargames.units.Unit;
import edu.ntnu.idatt2001.wargames.units.UnitFactory;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.stage.Stage;

import java.util.Optional;

/**
 * Controller class for the FXML in the edit unit dialog
 */
public class EditUnitDialogController {
    @FXML
    ChoiceBox unitType;

    @FXML
    TextField unitName, unitHealth;

    @FXML
    Button save, delete;

    /**
     * Initialize method that is run when the scene is set.
     */
    @FXML
    private void initialize() {
        for (UnitType u : UnitType.values()) {
            unitType.getItems().add(u);
        }
        unitType.setValue(UnitHandlerSingleton.getUnit2().getType());
        unitType.setStyle("-fx-font-size: 16px");
        unitName.setText(UnitHandlerSingleton.getUnit2().getName());
        unitHealth.setText(Integer.toString(UnitHandlerSingleton.getUnit2().getHealth()));
        UnitHandlerSingleton.setUnit2(null);
    }

    /**
     * Method that handles events on the save button.
     * Saves the unit and closes the dialog.
     */
    @FXML
    private void save() {
        UnitType type = (UnitType) unitType.getValue();
        String name = unitName.getText();
        int health;
        try {
            health = Integer.parseInt(unitHealth.getText());
        } catch (NumberFormatException e) {
            Alert a = new Alert(Alert.AlertType.ERROR);
            a.setContentText("The unit health must be a number");
            a.show();
            return;
        }
        if (health <= 0) {
            Alert a = new Alert(Alert.AlertType.ERROR);
            a.setContentText("The unit health must be greater than 0");
            a.show();
            return;
        }
        UnitHandlerSingleton.setUnit1(UnitFactory.makeUnit(type,name,health));
        unitType.setValue("UnitType");
        Stage stage  = (Stage) save.getScene().getWindow();
        stage.close();
    }

    /**
     * Method that handles events on the delete button.
     * Deletes the unit and closes the dialog.
     */
    @FXML
    private void delete() {
        Alert a = new Alert(Alert.AlertType.CONFIRMATION);
        a.setContentText("Are you sure you want to delete this unit? This action cannot be undone.");
        Optional<ButtonType> result = a.showAndWait();
        if (result.get() == ButtonType.OK) {
            UnitHandlerSingleton.setDelete(true);
            Stage stage  = (Stage) delete.getScene().getWindow();
            stage.close();
        }
    }
}
