package edu.ntnu.idatt2001.wargames;

import edu.ntnu.idatt2001.wargames.gui.GUI;
import javafx.application.Application;

/**
 *  Main class that launches the GUI application
 */
public class Main {
    public static void main(String[] args) {
            Application.launch(GUI.class,args);
        }
}
