package edu.ntnu.idatt2001.wargames.enums;

/**
 * Enum that contains three different terrains
 */
public enum Terrain {
    HILL,
    PLAINS,
    FOREST
}
