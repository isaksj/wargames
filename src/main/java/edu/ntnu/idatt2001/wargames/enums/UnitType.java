package edu.ntnu.idatt2001.wargames.enums;

/**
 * Enum that contains the four different unit types
 */
public enum UnitType {
    CavalryUnit,
    CommanderUnit,
    InfantryUnit,
    RangedUnit
}
